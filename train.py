# -*- coding: utf-8 -*-
import tkinter as tk
from tkinter import Message ,Text, filedialog
import cv2,os
import shutil
import csv
import numpy as np
from PIL import Image, ImageTk
import pandas as pd
import datetime
import time
import tkinter.ttk as ttk
import tkinter.font as font



import imutils
import pickle


window = tk.Tk()
#helv36 = tk.Font(family='Helvetica', size=36, weight='bold')
window.title("Face_Recogniser")

dialog_title = 'QUIT'
dialog_text = 'Are you sure?'
#answer = messagebox.askquestion(dialog_title, dialog_text)
 
window.geometry('1280x720')
window.configure(background='#153b3b')

#window.attributes('-fullscreen', True)

window.grid_rowconfigure(0, weight=1)
window.grid_columnconfigure(0, weight=1)

#path = "profile.jpg"

#Creates a Tkinter-compatible photo image, which can be used everywhere Tkinter expects an image object.
#img = ImageTk.PhotoImage(Image.open(path))

#The Label widget is a standard Tkinter widget used to display a text or image on the screen.
#panel = tk.Label(window, image = img)


#panel.pack(side = "left", fill = "y", expand = "no")

#cv_img = cv2.imread("img541.jpg")
#x, y, no_channels = cv_img.shape
#canvas = tk.Canvas(window, width = x, height =y)
#canvas.pack(side="left")
#photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(cv_img)) 
# Add a PhotoImage to the Canvas
#canvas.create_image(0, 0, image=photo, anchor=tk.NW)

#msg = Message(window, text='Hello, world!')

# Font is a tuple of (font_family, size_in_points, style_modifier_string)



message = tk.Label(window, text="Face Recognition Based Attendance" ,bg="#a53b3b"  ,fg="white"  ,width=50  ,height=3,font=("Courier New",30,'bold')) 
message.place(x=200, y=20)

lbl = tk.Label(window, text="Your University ID(Exmp:CSE00705396)",width=35  ,height=2  ,fg="red"  ,bg="yellow" ,font=('times', 15, ' bold ') ) 
lbl.place(x=200, y=200)

txt = tk.Entry(window,width=28  ,bg="yellow" ,fg="red",font=('times', 15, ' bold '))
txt.place(x=600, y=215)

lbl2 = tk.Label(window, text="Enter Your Name",width=28,fg="red",bg="yellow",height=2 ,font=('times', 15, ' bold ')) 
lbl2.place(x=200, y=300)

txt2 = tk.Entry(window,width=28  ,bg="yellow"  ,fg="red",font=('times', 15, ' bold ')  )
txt2.place(x=600, y=315)

lbl3 = tk.Label(window, text="Notification : ",width=20  ,fg="red"  ,bg="yellow"  ,height=2 ,font=('times', 15, ' bold ')) 
lbl3.place(x=400, y=400)

message = tk.Label(window, text="" ,bg="yellow"  ,fg="red"  ,width=50  ,height=2, activebackground = "yellow" ,font=('times', 15, ' bold ')) 
message.place(x=700, y=400)

lbl3 = tk.Label(window, text="Attendance : ",width=20  ,fg="red"  ,bg="yellow"  ,height=2 ,font=('times', 15, ' bold')) 
lbl3.place(x=400, y=650)


message2 = tk.Label(window, text="" ,fg="red"   ,bg="yellow",activeforeground = "green",width=30  ,height=2  ,font=('times', 15, ' bold ')) 
message2.place(x=700, y=650)
 
def clear():
    txt.delete(0, 'end')    
    res = ""
    message.configure(text= res)

def clear2():
    txt2.delete(0, 'end')    
    res = ""
    message.configure(text= res)    
    
def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
 
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
 
    return False
 
def TakeImages():        
    Id=(txt.get())
    name=(txt2.get())
    nameone=(str(txt2.get()).replace(" ", ""))
    if nameone.isalpha():
        cam = cv2.VideoCapture(0)
        detector=cv2.CascadeClassifier("./haarcascade_frontalface_default.xml")
        sampleNum=0
        folder_name = os.getcwd()+"/dataset/"+Id+"-"+name
        if not os.path.exists(folder_name):
            os.mkdir(folder_name)
        while(True):
            ret, img = cam.read()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            faces = detector.detectMultiScale(gray, 1.3, 5)
	    
            for (x,y,w,h) in faces:
                # cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)        
            
                #incrementing sample number
                sampleNum=sampleNum+1
                #saving the captured face in the dataset folder TrainingImage
                print(name)
                cv2.imwrite(folder_name+"/"+name +"."+Id +'.'+ str(sampleNum) + ".jpg", img)
                print("write")
                res = "Image taken {}/31".format(sampleNum)
                message.configure(text= res)
                
            #display the frame
            cv2.imshow('frame',img)
            #wait for 100 miliseconds
            #wait for 100 miliseconds 
            if cv2.waitKey(100) & 0xFF == ord('q'):
                break
            # break if the sample number is morethan 100
            elif sampleNum>30:
                break
        cam.release()
        cv2.destroyAllWindows() 
        res = "Images Saved for ID : " + Id +" Name : "+ name
        row = [Id , name]
        with open('./StudentDetails/StudentDetails.csv','a+') as csvFile:
            writer = csv.writer(csvFile)
            writer.writerow(row)
        csvFile.close()
        message.configure(text= res)
    else:
        if(is_number(Id)):
            res = "Enter Alphabetical Name"
            message.configure(text= res)
        if(name.isalpha()):
            res = "Enter Numeric Id"
            message.configure(text= res)
    
def TrainImages():
    #recognizer = cv2.face.LBPHFaceRecognizer_create()#$cv2.createLBPHFaceRecognizer()
    #harcascadePath = "haarcascade_frontalface_default.xml"
    #detector =cv2.CascadeClassifier(harcascadePath)
    #faces,Id = getImagesAndLabels("TrainingImage")
    #recognizer.train(faces, np.array(Id))
    #recognizer.save('recognizer/trainningData.yml')
    #recognizer.write("trainningData.yml")
    os.system("python extract_embeddings.py --dataset dataset --embeddings output/embeddings.pickle --detector face_detection_model --embedding-model openface_nn4.small2.v1.t7")
    os.system("python train_model.py --embeddings output/embeddings.pickle --recognizer output/recognizer.pickle --le output/le.pickle")
    res = "Image Trained"#+",".join(str(f) for f in Id)
    message.configure(text= res)

def getImagesAndLabels(path):
    #get the path of all the files in the folder
    imagePaths=[os.path.join(path,f) for f in os.listdir(path)] 
    #print(imagePaths)
    
    #create empth face list
    faces=[]
    #create empty ID list
    Ids=[]
    #now looping through all the image paths and loading the Ids and the images
    for imagePath in imagePaths:
        #loading the image and converting it to gray scale
        pilImage=Image.open(imagePath).convert('L')
        #Now we are converting the PIL image into numpy array
        imageNp=np.array(pilImage,'uint8')
        #getting the Id from the image
        Id=int(os.path.split(imagePath)[-1].split(".")[1])
        # extract the face from the training image sample
        faces.append(imageNp)
        Ids.append(Id)        
    return faces,Ids

def TrackImages():
    filename = filedialog.askopenfilename()
    print(filename)
    # os.system("python recognize_video.py --detector face_detection_model --embedding-model openface_nn4.small2.v1.t7 --recognizer output/recognizer.pickle --le output/le.pickle")
    # os.system("python recognize.py --detector face_detection_model --embedding-model openface_nn4.small2.v1.t7 --recognizer output/recognizer.pickle --le output/le.pickle --image images/Sraboni.jpg")
    # load our serialized face detector from disk
    df=pd.read_csv("./StudentDetails/StudentDetails.csv")
    col_names =  ['Id','Name','Date','Time']
    attendance = pd.DataFrame(columns = col_names)   

    print("[INFO] loading face detector...")
    protoPath = os.path.sep.join(["face_detection_model", "deploy.prototxt"]) 
    modelPath = os.path.sep.join(["face_detection_model",
        "res10_300x300_ssd_iter_140000.caffemodel"])
    detector = cv2.dnn.readNetFromCaffe(protoPath, modelPath)
    person_found = True
    # load our serialized face embedding model from disk
    print("[INFO] loading face recognizer...")
    embedder = cv2.dnn.readNetFromTorch("openface_nn4.small2.v1.t7")

    # load the actual face recognition model along with the label encoder
    recognizer = pickle.loads(open("output/recognizer.pickle", "rb").read())
    le = pickle.loads(open("output/le.pickle", "rb").read())

    # load the image, resize it to have a width of 600 pixels (while
    # maintaining the aspect ratio), and then grab the image dimensions
    image = cv2.imread(filename)
    image = imutils.resize(image, width=600)
    (h, w) = image.shape[:2]
    print(image)
    # construct a blob from the image
    imageBlob = cv2.dnn.blobFromImage(
        cv2.resize(image, (300, 300)), 1.0, (300, 300),
        (104.0, 177.0, 123.0), swapRB=False, crop=False)

    # apply OpenCV's deep learning-based face detector to localize
    # faces in the input image
    detector.setInput(imageBlob)
    detections = detector.forward()

    # loop over the detections
    for i in range(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with the
        # prediction
        confidence = detections[0, 0, i, 2]

        # filter out weak detections
        if confidence > 0.5:
            # compute the (x, y)-coordinates of the bounding box for the
            # face
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # extract the face ROI
            face = image[startY:endY, startX:endX]
            (fH, fW) = face.shape[:2]

            # ensure the face width and height are sufficiently large
            if fW < 20 or fH < 20:
                continue

            # construct a blob for the face ROI, then pass the blob
            # through our face embedding model to obtain the 128-d
            # quantification of the face
            faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255, (96, 96),
                (0, 0, 0), swapRB=True, crop=False)
            embedder.setInput(faceBlob)
            vec = embedder.forward()

            # perform classification to recognize the face
            preds = recognizer.predict_proba(vec)[0]
            j = np.argmax(preds)
            proba = preds[j]
            name = le.classes_[j]

            # draw the bounding box of the face along with the associated
            # probability
            text = "{}: {:.2f}%".format(name, proba * 100)
            y = startY - 10 if startY - 10 > 10 else startY + 10
            cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0, 255), 2)
            cv2.putText(image, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
            print(name)
    
            ts = time.time()
            date = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
            timeStamp = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
            if name == "unknown":
                person_found = False
            else:
                id,name= name.split("-")
                

            attendance.loc[len(attendance)] = [id,name,date,timeStamp]
    # show the output image
    cv2.imshow("Image", image)
    

    attendance=attendance.drop_duplicates(subset=['Id'],keep='first')

    ts = time.time()      
    date = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    timeStamp = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
    Hour,Minute,Second=timeStamp.split(":")
    fileName="./Attendance/Attendance_"+date+"_"+Hour+"-"+Minute+"-"+Second+".csv"
    attendance.to_csv(fileName,index=False)

    print(attendance)
    res=attendance
    print("Result",res)
    message.configure(text= res)
    cv2.waitKey(0)
    cv2.destroyAllWindows() 

    # recognizer = cv2.face.LBPHFaceRecognizer_create()#cv2.createLBPHFaceRecognizer()
    # recognizer.read("trainningData.yml")
    # harcascadePath = "haarcascade_frontalface_default.xml"
    # faceCascade = cv2.CascadeClassifier(harcascadePath);    
    # df=pd.read_csv("./StudentDetails/StudentDetails.csv")
    # cam = cv2.VideoCapture(0)
    # font = cv2.FONT_HERSHEY_SIMPLEX        
    # col_names =  ['Id','Name','Date','Time']
    # attendance = pd.DataFrame(columns = col_names)    
    # while True:
    #     ret, im =cam.read()
    #     gray=cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)
    #     faces=faceCascade.detectMultiScale(gray, 1.2,5)    
    #     for(x,y,w,h) in faces:
    #         cv2.rectangle(im,(x,y),(x+w,y+h),(225,0,0),2)
    #         Id, conf = recognizer.predict(gray[y:y+h,x:x+w])                                   
    #         if(conf < 50):
    #             ts = time.time()      
    #             date = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    #             timeStamp = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
    #             aa=df.loc[df['Id'] == Id]['Name'].values
    #             tt=str(Id)+"-"+aa
    #             attendance.loc[len(attendance)] = [Id,aa,date,timeStamp]
                
    #         else:
    #             Id='Unknown'                
    #             tt=str(Id)  
    #         if(conf > 75):
    #             noOfFile=len(os.listdir("ImagesUnknown"))+1
    #             cv2.imwrite("./ImagesUnknown/Image"+str(noOfFile) + ".jpg", im[y:y+h,x:x+w])            
    #         cv2.putText(im,str(tt),(x,y+h), font, 1,(255,255,255),2)        
    #     attendance=attendance.drop_duplicates(subset=['Id'],keep='first')    
    #     cv2.imshow('im',im) 
    #     if (cv2.waitKey(1)==ord('q')):
    #         break
    # ts = time.time()      
    # date = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')
    # timeStamp = datetime.datetime.fromtimestamp(ts).strftime('%H:%M:%S')
    # Hour,Minute,Second=timeStamp.split(":")
    # fileName="./Attendance/Attendance_"+date+"_"+Hour+"-"+Minute+"-"+Second+".csv"
    # attendance.to_csv(fileName,index=False)
    # cam.release()
    # cv2.destroyAllWindows()
    # #print(attendance)
    # res=attendance
    # message2.configure(text= res)

  
clearButton = tk.Button(window, text="Clear", command=clear  ,fg="red"  ,bg="yellow"  ,width=28  ,height=2 ,activebackground = "Red" ,font=('times', 15, ' bold '))
clearButton.place(x=950, y=200)
clearButton2 = tk.Button(window, text="Clear", command=clear2  ,fg="red"  ,bg="yellow"  ,width=28  ,height=2, activebackground = "Red" ,font=('times', 15, ' bold '))
clearButton2.place(x=950, y=300)    
takeImg = tk.Button(window, text="Take Images", command=TakeImages  ,fg="red"  ,bg="yellow"  ,width=20  ,height=3, activebackground = "Red" ,font=('times', 15, ' bold '))
takeImg.place(x=200, y=500)
trainImg = tk.Button(window, text="Train Images", command=TrainImages  ,fg="red"  ,bg="yellow"  ,width=20  ,height=3, activebackground = "Red" ,font=('times', 15, ' bold '))
trainImg.place(x=500, y=500)
trackImg = tk.Button(window, text="Track Images", command=TrackImages  ,fg="red"  ,bg="yellow"  ,width=20  ,height=3, activebackground = "Red" ,font=('times', 15, ' bold '))
trackImg.place(x=800, y=500)
quitWindow = tk.Button(window, text="Quit", command=window.destroy  ,fg="red"  ,bg="yellow"  ,width=20  ,height=3, activebackground = "Red" ,font=('times', 15, ' bold '))
quitWindow.place(x=1100, y=500)
 
window.mainloop()
